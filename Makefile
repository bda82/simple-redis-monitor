build:
	docker-compose up --build
run:
	docker-compose up -d --build
down:
	docker-compose down
log:
	docker logs ex_redis -f
rl:
	make run
	make log