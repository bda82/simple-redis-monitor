import re


class BaseError(Exception):

    def __init__(self, message: str = '', file: str = None) -> None:
        super().__init__()
        self.name = self.__class__.__name__
        self.message = (
            message or ' '.join(re.findall('[A-Z][^A-Z]*', self.name)).capitalize()
        )

        self.file = file or __file__
        self.status = 402

    def __str__(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return self.message

    def json(self):
        return {'error': self.name,
                'message': self.message,
                'status': self.status}
