from .base_error import BaseError


class NoRedisConnectionError(BaseError):

    def __init__(self, message: str = 'No Redis connection Error!'):
        super().__init__(message, __file__)
