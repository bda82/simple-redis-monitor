from .base_error import BaseError


class NoRCallbackFunctionError(BaseError):

    def __init__(self, message: str = 'No Redis on_message Callback Function provided Error!'):
        super().__init__(message, __file__)
