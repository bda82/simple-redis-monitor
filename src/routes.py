from handlers import *


def setup_routes(app):
    app.router.add_view('', IndexHandler)
    app.router.add_view('/db/{db}', DBViewHandler)
    app.router.add_view('/db/{db}/{key}', DBKeyViewHandler)

    app.router.add_view('/list-all-db', ListAllDBHandler)
    app.router.add_view('/list-db/{db}', ListDBHandler)
    app.router.add_view('/get/{db}/{key}', GetHandler)
    app.router.add_view('/set/{db}', SetHandler)
    app.router.add_view('/queue/{db}', QueueHandler)
