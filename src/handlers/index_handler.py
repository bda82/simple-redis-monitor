import random
import json
import datetime
from json import JSONDecodeError
import jinja2
import aiohttp_jinja2

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class IndexHandler(web.View, CorsViewMixin):

    async def get(self):
        request = self.request

        context = {

        }

        return aiohttp_jinja2.render_template("index.html",
                                              request,
                                              context=context)


