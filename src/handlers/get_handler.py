import random
import json
import datetime
from json import JSONDecodeError

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class GetHandler(web.View, CorsViewMixin):

    async def get(self):
        request = self.request

        db = request.match_info["db"]
        key = request.match_info["key"]

        r = RedisConnection(db_=db)
        value = await r.get(key)
        await r.close()

        response = {'data': {'db': db,
                             'key': key,
                             'value': value}}

        status_code = web.HTTPOk().status_code

        return web.json_response(data=response, status=status_code)
