import random
import json
import datetime
from json import JSONDecodeError
import jinja2
import aiohttp_jinja2

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class DBKeyViewHandler(web.View, CorsViewMixin):

    async def get(self):
        request = self.request
        db = self.request.match_info["db"]
        key = self.request.match_info["key"]
        print(f'{db = }, {key = }')

        keys = []
        try:
            r = RedisConnection(db_=db)
            keys = await r.keys()
            await r.close()
        except Exception as e:
            print(f'Redis connection error: {e = }')

        value = ''
        value_type = 'string'
        if key:
            try:
                r = RedisConnection(db_=db)
                value = await r.get(key)
                await r.close()
            except Exception as e:
                r = RedisConnection(db_=db)
                value = await r.lrange(key)
                print(type(value))
                value_type = 'list'
                await r.close()

        context = {
            'db_num': db,
            'keys': keys,
            'key': key,
            'value': value,
            'value_type': value_type
        }
        return aiohttp_jinja2.render_template("db.html",
                                              request,
                                              context=context)
