import random
import json
import datetime
from json import JSONDecodeError
import jinja2
import aiohttp_jinja2

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class DBViewHandler(web.View, CorsViewMixin):

    async def get(self):
        request = self.request
        db = self.request.match_info["db"]

        keys = []
        try:
            r = RedisConnection(db_=db)
            keys = await r.keys()
            await r.close()
        except Exception as e:
            print(f'Redis connection error: {e = }')

        context = {
            'db_num': db,
            'keys': keys
        }
        return aiohttp_jinja2.render_template("db.html",
                                              request,
                                              context=context)
