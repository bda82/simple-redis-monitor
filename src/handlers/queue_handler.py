import random
import json
import datetime
from json import JSONDecodeError

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class QueueHandler(web.View, CorsViewMixin):

    async def post(self, ):
        """
        POST JSON = {
            "command": "lpush",
            "queue": "queue",
            "values": [1, 2, 3]
        }
        :return:
        """
        response = {'data': {}}

        db = self.request.match_info["db"]

        try:
            request = await self.request.json()
        except JSONDecodeError:
            request_str = await self.request.text()
            request = json.loads(request_str)

        try:
            r = RedisConnection(db_=db)
            command = request.get('command')
            queue = request.get('queue')
            values = request.get('values', [])
            if command is not None:
                if command == 'lpush':
                    response['data'] = []
                    for v in values:
                        d = await r.lpush(queue, v)
                        response['data'].append(d)
                elif command == 'lpop':
                    response['data'] = await r.lpop(queue)
                elif command == 'rpush':
                    response['data'] = []
                    for v in values:
                        d = await r.rpush(queue, v)
                        response['data'].append(d)
                elif command == 'rpop':
                    response['data'] = await r.rpop(queue)
            await r.close()
        except Exception as e:
            print(f'Redis Connection Error: {e = }')

        status_code = web.HTTPOk().status_code

        return web.json_response(data=response, status=status_code)
