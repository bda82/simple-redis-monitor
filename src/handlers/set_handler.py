import random
import json
import datetime
from json import JSONDecodeError

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class SetHandler(web.View, CorsViewMixin):

    async def post(self, ):
        response = {'data': {}}

        db = self.request.match_info["db"]

        try:
            request = await self.request.json()
        except JSONDecodeError:
            request_str = await self.request.text()
            request = json.loads(request_str)

        try:
            r = RedisConnection(db_=db)

            for k in request.keys():
                await r.set(k, request[k])

            await r.close()
        except Exception as e:
            print(f'Redis Connection Error: {e = }')

        status_code = web.HTTPOk().status_code

        return web.json_response(data=response, status=status_code)
