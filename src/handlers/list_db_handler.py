import random
import json
import datetime
from json import JSONDecodeError

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class ListDBHandler(web.View, CorsViewMixin):

    async def get(self):
        request = self.request

        db = request.match_info["db"]
        response = {'data': {'db': db,
                             'keys': []}}

        try:
            r = RedisConnection(db_=db)
            response['data']['keys'] = await r.keys()
            await r.close()
        except Exception as e:
            print(f'Redis connection error: {e = }')

        status_code = web.HTTPOk().status_code

        return web.json_response(data=response, status=status_code)