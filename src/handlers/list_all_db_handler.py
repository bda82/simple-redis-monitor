import random
import json
import datetime
from json import JSONDecodeError

from aiohttp import web
from aiohttp_cors import CorsViewMixin

from connections.redis_connection import RedisConnection

redis_connection = RedisConnection()


class ListAllDBHandler(web.View, CorsViewMixin):

    async def get(self):
        response = {'data': {}}

        dbs_ = range(10)
        queues = {}

        for db_ in dbs_:
            try:
                r = RedisConnection(db_=db_)
                print(f'Connect to {db_ = } as {r = }')
                keys = await r.keys()
                queues[db_] = keys
                print(f'Get {keys = }')
                await r.close()
            except Exception as e:
                print(f'Redis Connection Error: {e = }')

        response['data']['queues'] = queues

        status_code = web.HTTPOk().status_code

        return web.json_response(data=response, status=status_code)
