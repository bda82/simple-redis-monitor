import asyncio
import aioredis
import async_timeout

from errors import *

from config import Config

redis_config = Config().redis_config


class RedisConnection:

    def __init__(self, db_: int = 0):
        self.db_ = db_
        self.cache = aioredis.Redis.from_url(redis_config.url + '/' + str(db_),
                                             encoding="utf-8",
                                             decode_responses=True)
        self.psub = self.cache.pubsub()
        self.stop_subscription = 'stop_subscription'

    async def close(self):
        if self.cache:
            return await self.cache.close()
        raise NoRedisConnectionError()

    async def keys(self, pattern='*'):
        if self.cache:
            return await self.cache.keys(pattern)
        raise NoRedisConnectionError()

    async def set(self, key, value):
        if self.cache:
            return await self.cache.set(key, value)
        raise NoRedisConnectionError()

    async def get(self, key):
        if self.cache:
            return await self.cache.get(key)
        raise NoRedisConnectionError()

    async def delete(self, key):
        if self.cache:
            return await self.cache.delete(key)
        raise NoRedisConnectionError()

    async def lpop(self, queue):
        if self.cache:
            return await self.cache.lpop(queue)
        raise NoRedisConnectionError()

    async def rpop(self, queue):
        if self.cache:
            return await self.cache.rpop(queue)
        raise NoRedisConnectionError()

    async def lpush(self, queue, value):
        if self.cache:
            return await self.cache.lpush(queue, value)
        raise NoRedisConnectionError()

    async def rpush(self, queue, value):
        if self.cache:
            return await self.cache.rpush(queue, value)
        raise NoRedisConnectionError()

    async def llen(self, queue):
        if self.cache:
            return await self.cache.llen(queue)
        raise NoRedisConnectionError()

    async def lrange(self, queue, start=0, end=-1):
        if self.cache:
            return await self.cache.lrange(queue, start, end)
        raise NoRedisConnectionError()

    async def reader(self, channel: aioredis.client.PubSub, callback=None):
        while True:
            try:
                async with async_timeout.timeout(1):
                    print('Ping PubSub...')
                    message = await channel.get_message(ignore_subscribe_messages=True)
                    if message is not None:
                        print(f"(Reader) Message Received: {message}")
                        if message["data"] == self.stop_subscription:
                            print("(Reader) STOP")
                            break
                        else:
                            if callback is None:
                                raise NoRCallbackFunctionError()
                            try:
                                await callback(message["data"])
                            except Exception as e:
                                print(f'Callback function error: {e = }')
                    await asyncio.sleep(0.01)
            except asyncio.TimeoutError:
                raise

    async def subscribe(self, channel_name: str = 'channel:1'):
        if not self.cache:
            raise NoRedisConnectionError()
        async with self.psub as p:
            await p.subscribe(channel_name)
            await self.reader(p)
            await p.unsubscribe(channel_name)
        await self.psub.close()

    async def publish(self, channel_name: str = 'channel:1', message = None):
        if not self.cache:
            raise NoRedisConnectionError()
        # wait for subscribers
        while True:
            subs = dict(await self.psub.pubsub_numsub(channel_name))
            if subs["channel:1"] == 1:
                break
            await asyncio.sleep(0.1)
        #
        if message is not None:
            print(f'Publish {message = }')
            await self.psub.publish(channel_name, message)

    async def stop_subscription(self, channel_name: str = 'channel:1'):
        if not self.cache:
            raise NoRedisConnectionError()
        # wait for subscribers
        while True:
            subs = dict(await self.psub.pubsub_numsub(channel_name))
            if subs["channel:1"] == 1:
                break
            await asyncio.sleep(0.1)
        await self.psub.publish(channel_name, self.stop_subscription)
