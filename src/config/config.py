from .server_config import ServerConfig
from .redis_config import RedisConfig


class Config:

    server_config = ServerConfig()
    redis_config = RedisConfig()
