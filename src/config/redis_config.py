

class RedisConfig:
    host = 'redis'
    port = 6379
    url = f"redis://{host}:{port}"
