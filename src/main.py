import os
import aiohttp_cors as aiohttp_cors
import uvloop
from aiohttp import web
import jinja2
import aiohttp_jinja2

from config import Config as ServerConfig

from routes import setup_routes

uvloop.install()
config = ServerConfig()


async def on_startup(app: web.Application = None):
    pass


def init_app():
    app = web.Application()
    app.on_startup.append(on_startup)

    setup_routes(app)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
            max_age=3600,
        )
    })

    for route in list(app.router.routes()):
        cors.add(route)

    aiohttp_jinja2.setup(
        app, loader=jinja2.FileSystemLoader(os.path.join(os.getcwd(), "templates"))
    )

    return app


def main():
    print('-' * 50)
    print('Start server')
    print('-' * 50)
    app = init_app()
    web.run_app(app=app,
                port=config.server_config.server_port)


if __name__ == '__main__':
    main()
